package com.example.lynn.historygame;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.lynn.historygame.MainActivity.*;

/**
 * Created by lynn on 6/14/2016.
 */
public class MyListener implements View.OnClickListener {
    private TextView first;

    public void scramble(String[] array) {
        for (int counter=0;counter<100;counter++) {
            int position1 = (int)(array.length*Math.random());

            int position2 = position1;

            while (position2 == position1)
               position2 = (int)(array.length*Math.random());

            String temp = array[position1];
            array[position1] = array[position2];
            array[position2] = temp;
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            Button source = (Button)v;

            if (source == play) {
                List<Integer> indices = new ArrayList<>();

                while (indices.size() < 4) {
                    int index = (int)(presidents.size()*Math.random());

                    if (!indices.contains(index))
                        indices.add(index);
                }

                names = new String[4];

                scrambledNames = new String[4];

                for (int counter=0;counter<indices.size();counter++) {
                    President president = presidents.get(indices.get(counter));

                    Drawable drawable = v.getContext().getResources().getDrawable(president.getId());

                    views[counter].setImageDrawable(drawable);

                    names[counter] = president.getName();

                    scrambledNames[counter] = president.getName();
                }

                scramble(scrambledNames);

                for (int counter=0;counter<scrambledNames.length;counter++)
                    textViews[counter].setText(scrambledNames[counter]);
            } else if (source == submit) {
                if (Arrays.equals(names,scrambledNames)) {
                    message.setText("Right");

                    score += 10;
                } else {
                    message.setText("Wrong");

                    score -= 10;
                }

                scoreView.setText("Score: " + score);

                for (int counter=0;counter<views.length;counter++) {
                    views[counter].setImageDrawable(null);

                    textViews[counter].setText("");
                }


            }
        } else if (v instanceof TextView) {
            TextView source = (TextView)v;

            if (first == null)
                first = source;
            else {
                TextView second = source;

                String temp = first.getText().toString();
                first.setText(second.getText());
                second.setText(temp);

                first = null;
            }

            for (int counter=0;counter<textViews.length;counter++)
                scrambledNames[counter] = textViews[counter].getText().toString();
        }

    }

}
