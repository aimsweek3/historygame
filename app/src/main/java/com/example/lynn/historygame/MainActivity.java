package com.example.lynn.historygame;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends ActionBarActivity {
    public static ImageView[] views;
    public static TextView[] textViews;
    public static MyListener listener = new MyListener();
    public static Button play;
    public static Button submit;
    public static GamePanel gamePanel;
    public static ControlPanel controlPanel;
    public static int width;
    public static int height;
    public static ImageView[] digits;
    public static SQLiteDatabase database;
    public static MyDatabaseHelper helper;
    public static List<President> presidents;
    public static TextView scoreView;
    public static TextView message;
    public static String[] names;
    public static String[] scrambledNames;
    public static int score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = new MyDatabaseHelper(this);

        database = helper.getReadableDatabase();

        presidents = President.getPresidents();

        setContentView(new MyView(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
