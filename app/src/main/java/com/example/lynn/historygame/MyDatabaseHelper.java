package com.example.lynn.historygame;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lynn on 6/14/2016.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Presidents1";

    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE = "CREATE TABLE presidents(president TEXT NOT NULL,startdate TEXT NOT NULL,enddate TEXT NOT NULL,drawable INTEGER NOT NULL);";

    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);

        db.execSQL("INSERT INTO presidents VALUES('George Washington','April 30, 1789','March 4, 1797'," + R.drawable.washington + ");");
        db.execSQL("INSERT INTO presidents VALUES('John Adams','March 4, 1797','March 4, 1801'," + R.drawable.adams + ");");
        db.execSQL("INSERT INTO presidents VALUES('Thomas Jefferson','March 4, 1801','March 4, 1809'," + R.drawable.jefferson + ");");
        db.execSQL("INSERT INTO presidents VALUES('James Madison','March 4, 1809','March 4, 1817'," + R.drawable.madison + ");");
        db.execSQL("INSERT INTO presidents VALUES('James Monroe','March 4, 1817','March 4, 1825'," + R.drawable.monroe + ");");
        db.execSQL("INSERT INTO presidents VALUES('John Quincy Adams','March 4, 1825','March 4, 1829'," + R.drawable.quincyadams + ");");
        db.execSQL("INSERT INTO presidents VALUES('Andrew Jackson','March 4, 1829','March 4, 1837'," + R.drawable.jackson + ");");
        db.execSQL("INSERT INTO presidents VALUES('Martin Van Buren','March 4, 1837','March 4, 1841'," + R.drawable.vanburen + ");");
        db.execSQL("INSERT INTO presidents VALUES('William Henry Harrison','March 4, 1841','April 4, 1841'," + R.drawable.henryharrison + ");");
        db.execSQL("INSERT INTO presidents VALUES('John Tyler','April 4, 1841','March 4, 1845'," + R.drawable.tyler + ");");
        db.execSQL("INSERT INTO presidents VALUES('James K. Polk','March 4, 1845','March 4, 1849'," + R.drawable.polk + ");");
        db.execSQL("INSERT INTO presidents VALUES('Zachary Taylor','March 4, 1849','July 9, 1850'," + R.drawable.taylor + ");");
        db.execSQL("INSERT INTO presidents VALUES('Millard Fillmore','July 9, 1850','March 4, 1853'," + R.drawable.fillmore + ");");
        db.execSQL("INSERT INTO presidents VALUES('Franklin Pierce','March 4, 1853','March 4, 1857'," + R.drawable.pierce + ");");
        db.execSQL("INSERT INTO presidents VALUES('James Buchanan','March 4, 1857','March 4, 1861'," + R.drawable.buchanan + ");");
        db.execSQL("INSERT INTO presidents VALUES('Abraham Lincoln','March 4, 1861','April 15, 1865'," + R.drawable.lincoln + ");");
        db.execSQL("INSERT INTO presidents VALUES('Andrew Johnson','April 15, 1865','March 4, 1869'," + R.drawable.andrewjohnson + ");");
        db.execSQL("INSERT INTO presidents VALUES('Ulysses S. Grant','March 4, 1869','March 4, 1877'," + R.drawable.grant + ");");
        db.execSQL("INSERT INTO presidents VALUES('Rutherford B. Hayes','March 4, 1877','March 4, 1881'," + R.drawable.hayes + ");");
        db.execSQL("INSERT INTO presidents VALUES('James A. Garfield','March 4, 1881','September 19, 1881'," + R.drawable.garfield + ");");
        db.execSQL("INSERT INTO presidents VALUES('Chester A. Arthur','September 19, 1881','March 4, 1885'," + R.drawable.arthur + ");");
        db.execSQL("INSERT INTO presidents VALUES('Grover Cleveland','March 4, 1885','March 4, 1889'," + R.drawable.cleveland + ");");
        db.execSQL("INSERT INTO presidents VALUES('Benjamin Harrison','March 4, 1889','March 4, 1893'," + R.drawable.benjaminharrison + ");");
        db.execSQL("INSERT INTO presidents VALUES('Grover Cleveland','March 4, 1893','March 4, 1897'," + R.drawable.cleveland + ");");
        db.execSQL("INSERT INTO presidents VALUES('William McKinley','March 4, 1897','September 14, 1901'," + R.drawable.mckinley + ");");
        db.execSQL("INSERT INTO presidents VALUES('Theodore Roosevelt','September 14, 1901','March 4, 1909'," + R.drawable.theodoreroosevelt + ");");
        db.execSQL("INSERT INTO presidents VALUES('William Howard Taft','March 4, 1909','March 4, 1913'," + R.drawable.taft + ");");
        db.execSQL("INSERT INTO presidents VALUES('Woodrow Wilson','March 4, 1913','March 4, 1921'," + R.drawable.wilson + ");");
        db.execSQL("INSERT INTO presidents VALUES('Warren G. Harding','March 4, 1921','August 2, 1923'," + R.drawable.harding + ");");
        db.execSQL("INSERT INTO presidents VALUES('Calvin Coolidge','August 2, 1923','March 4, 1929'," + R.drawable.coolidge + ");");
        db.execSQL("INSERT INTO presidents VALUES('Herbert Hoover','March 4, 1929','March 4, 1933'," + R.drawable.hoover + ");");
        db.execSQL("INSERT INTO presidents VALUES('Franklin D. Roosevelt','March 4, 1933','April 12, 1945'," + R.drawable.franklinroosevelt + ");");
        db.execSQL("INSERT INTO presidents VALUES('Harry S. Truman','April 12, 1945','January 20, 1953'," + R.drawable.truman + ");");
        db.execSQL("INSERT INTO presidents VALUES('Dwight D. Eisenhower','January 20, 1953','January 20, 1961'," + R.drawable.eisenhower + ");");
        db.execSQL("INSERT INTO presidents VALUES('John F. Kennedy','January 20, 1961','November 22, 1963'," + R.drawable.kennedy + ");");
        db.execSQL("INSERT INTO presidents VALUES('Lyndon B. Johnson','November 22, 1963','January 20, 1969'," + R.drawable.lyndonjohnson + ");");
        db.execSQL("INSERT INTO presidents VALUES('Richard Nixon','January 20, 1969','August 9, 1974'," + R.drawable.nixon + ");");
        db.execSQL("INSERT INTO presidents VALUES('Gerald Ford','August 9, 1974','January 20, 1977'," + R.drawable.ford + ");");
        db.execSQL("INSERT INTO presidents VALUES('Jimmy Carter','January 20, 1977','January 20, 1981'," + R.drawable.carter + ");");
        db.execSQL("INSERT INTO presidents VALUES('Ronald Reagan','January 20, 1981','January 20, 1989'," + R.drawable.reagen + ");");
        db.execSQL("INSERT INTO presidents VALUES('George H. W. Bush','January 20, 1989','January 20, 1993'," + R.drawable.bush + ");");
        db.execSQL("INSERT INTO presidents VALUES('Bill Clinton','January 20, 1993','January 20, 2001'," + R.drawable.clinton + ");");
        db.execSQL("INSERT INTO presidents VALUES('George W. Bush','January 20, 2001','January 20, 2009'," + R.drawable.georgewbush + ");");
        db.execSQL("INSERT INTO presidents VALUES('Barack Obama','January 20, 2009','-'," + R.drawable.obama + ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
