package com.example.lynn.historygame;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.example.lynn.historygame.MainActivity.*;

/**
 * Created by lynn on 6/14/2016.
 */
public class ControlPanel extends LinearLayout {

    public ControlPanel(Context context) {
        super(context);

        play = new Button(context);

        submit = new Button(context);

        play.setOnClickListener(listener);
        submit.setOnClickListener(listener);

        play.generateViewId();

        submit.generateViewId();

        play.setBackground(getResources().getDrawable(R.drawable.cat));

        submit.setBackground(getResources().getDrawable(R.drawable.duck));

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200,200);

        play.setLayoutParams(layoutParams);
        
        submit.setLayoutParams(layoutParams);

        addView(play);
        addView(submit);

        scoreView = new TextView(context);

        layoutParams = new LinearLayout.LayoutParams(500,200);

        scoreView.setText("Score: " + score);

        scoreView.setTextSize(50);

        scoreView.setLayoutParams(layoutParams);

        scoreView.setTypeface(Typeface.create("Serif",Typeface.BOLD));

        scoreView.setTranslationX(50);

        addView(scoreView);

        message = new TextView(context);

        message.setText("Message");

        message.setTextSize(40);

        message.setTranslationX(500);

        addView(message);

        play.setOnClickListener(listener);
        submit.setOnClickListener(listener);
    }

}
