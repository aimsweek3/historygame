package com.example.lynn.historygame;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import android.widget.TableLayout;
import android.widget.TableRow;

import static com.example.lynn.historygame.MainActivity.*;

/**
 * Created by lynn on 6/14/2016.
 */
public class MyView extends TableLayout {

    public MyView(Context context) {
        super(context);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();;
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        gamePanel = new GamePanel(context);

        controlPanel = new ControlPanel(context);

        TableRow row1 = new TableRow(context);

        row1.addView(controlPanel);

        controlPanel.setLayoutParams(new TableRow.LayoutParams(width,height/8));

        TableRow row2 = new TableRow(context);

        row2.addView(gamePanel);

        gamePanel.setLayoutParams(new TableRow.LayoutParams(width,(6*height)/8));

        controlPanel.setBackgroundColor(0xFFFF0000);

        gamePanel.setBackgroundColor(0xFF00FF00);

        addView(row1);
        addView(row2);
    }

}
